﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPIDemo.Models;
using AutoMapper;

namespace WebAPIDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalController : ControllerBase
    {
        private readonly AnimalContext _context;
        private readonly IMapper _mapper;

        public AnimalController(AnimalContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

            if (_context.Animals.Count() == 0)
            {
                // Create a new animal if collection is empty,
                // which means you can't delete all animals.
                AnimalFactory factory = new ConcreteAnimalFactory();

                _context.Animals.Add(
                    factory.CreateAnimal(id: "Spot", type: "dog")
                );

                _context.SaveChanges();
            }
        }

        // GET api/animal
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Animal>>> GetAnimals()
        {
            return await _context.Animals.ToListAsync();
        }


        // GET api/animal/Spot
        [HttpGet("{id}")]
        public async Task<ActionResult<Animal>> GetAnimal(string id)
        {
            var animal = await _context.Animals.FindAsync(id);

            if (animal == null)
                return NotFound();
            else {
                return animal;
            }
        }

        // GET api/animal/Spot/speak
        [HttpGet("{id}/speak")]
        public async Task<ActionResult<string>> AnimalSpeak(string id)
        {
            var animal = await _context.Animals.FindAsync(id);


            if (animal == null)
                return NotFound();

            if (animal.Type == "dog")
            {
                Dog dog = _mapper.Map<Animal, Dog>(animal);
                return dog.Speak();
            }
            else if (animal.Type == "cat")
            {
                Cat cat = _mapper.Map<Animal, Cat>(animal);
                return cat.Speak();
            }
            else
            {
                return animal.Speak();
            }
        }

        //POST api/animal
        [HttpPost]
        public async Task<ActionResult<Animal>> BoardAnimal([FromBody]Animal animal)
        {
            _context.Animals.Add(animal);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetAnimal), new { id = animal.Id }, animal);
        }

        // PUT api/anima/Spot
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, [FromBody]Animal animal)
        {
            if (id != animal.Id)
                return BadRequest();

            animal.Type = animal.Type.ToLower();

            _context.Entry(animal).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE: api/animal/Spot
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem(string id)
        {
            var animal = await _context.Animals.FindAsync(id);

            if (animal == null)
                return NotFound();

            _context.Animals.Remove(animal);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
