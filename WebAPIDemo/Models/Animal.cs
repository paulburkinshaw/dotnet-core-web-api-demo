﻿namespace WebAPIDemo.Models
{
    public class Animal : IAnimal // Product
    {
        public Animal() { }

        public Animal(string id)
        {
            Id = id;
        }

        public Animal(string id, string type)
        {
            Id = id;
            Type = type.ToLower();
        }

        public string Id { get; set; }
        public string Type { get; set; }

        public virtual string Speak() {
            return $"{Id} says ...!";
        }
    }
}
