﻿using Microsoft.EntityFrameworkCore;


namespace WebAPIDemo.Models
{
    public class AnimalContext : DbContext
    {
        public AnimalContext(DbContextOptions<AnimalContext> options)
            : base(options)
        {
        }

        public DbSet<Animal> Animals { get; set; }
    }
}