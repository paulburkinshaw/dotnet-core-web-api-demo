﻿namespace WebAPIDemo.Models
{
    public abstract class AnimalFactory // Creator
    {
        public abstract Animal CreateAnimal(string id, string type);
    }
}
