﻿namespace WebAPIDemo.Models
{
    public class ConcreteAnimalFactory : AnimalFactory // Concrete creator
    {
        public override Animal CreateAnimal(string id, string type)
        {
            switch (type.ToLower()) {
                case "dog":
                    return new Dog(id: id);
                case "cat":
                    return new Cat(id: id);
                default:
                    return new Animal(id: id);
            }
        }
    }
}
