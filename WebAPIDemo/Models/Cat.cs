﻿namespace WebAPIDemo.Models
{
    public class Cat : Animal, IAnimal // ConcreteProduct
    {
        public Cat(string id)
        {
            Id = id;
            Type = "cat";
        }

        public override string Speak()
        {
            return $"{Id} says MEOW!";
        }
    }
}
